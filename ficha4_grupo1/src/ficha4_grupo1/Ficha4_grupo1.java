
package ficha4_grupo1;

import java.awt.BorderLayout;
import java.util.Scanner;
import java.util.EventObject;
import java.util.Random;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.ArrayList;
import static javafx.scene.input.KeyCode.M;

public class Ficha4_grupo1 {

    public static void main(String[] args) {
        
        int[][] matriz = new int[3][4];
        int arrayFreq [] = new int[11];
        
        Scanner entrada = new Scanner(System.in);
        System.out.println("Matriz M[3][4]\n");
        
        for(int linha=0 ; linha < 3 ; linha++){
            for(int coluna = 0; coluna < 4 ; coluna ++){
                System.out.printf("Introduza um número M[%d][%d]: ",linha+1,coluna+1);
                matriz[linha][coluna]=entrada.nextInt();
            }
        }
            
        System.out.println("\nMatriz: \n");
        
        for(int linha=0 ; linha < 3 ; linha++){
                
            for(int coluna = 0; coluna < 4 ; coluna ++){
                    
                System.out.printf("\t %d \t",matriz[linha][coluna]);
                    
            }
                
            System.out.println();
                
        }
        
        for (int linha=0; linha<3; linha++){
                
                System.out.println("Elementos da linha: " +linha);
                
                for(int coluna=0; coluna<4; coluna++){
                
                System.out.println(matriz[linha][coluna]);
                
                }
            }
        
        for(int linha=0; linha<matriz.length;linha++){
            for(int coluna=0; coluna<matriz[linha].length;coluna++){      
                //soma 1 no array para cada valor que aparece na matriz
                arrayFreq[matriz[linha][coluna]]++;     
            }  
        }
        
        //mostra o resultado
        for (int i=0; i<arrayFreq.length;i++){
                
            System.out.printf("%02d: %d\n", i,arrayFreq[i]);
                        
        }
    }
    
}
